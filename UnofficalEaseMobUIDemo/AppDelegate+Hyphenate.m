//
//  AppDelegate+Hyphenate.m
//  UnofficalEaseMobUIDemo
//
//  Created by z on 2017/6/21.
//  Copyright © 2017年 z. All rights reserved.
//

#import "AppDelegate+Hyphenate.h"
#import "EaseUI.h"

#define APPKEY @"easemob-demo#chatdemoui"
#define APNSNAME @"easemobuidemoAnpsName"

@implementation AppDelegate (Hyphenate)

- (void)setupHyphenateWihtApplication:(UIApplication *)application options:(NSDictionary *)launchOptions {
    // 初始化sdk
    EMOptions *options = [EMOptions optionsWithAppkey:APPKEY];
    options.apnsCertName = APNSNAME;
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    // 注册3.xSDK，注册远程通知
    [[EaseSDKHelper shareHelper] hyphenateApplication:application
                        didFinishLaunchingWithOptions:launchOptions
                                               appkey:APPKEY
                                         apnsCertName:APNSNAME
                                          otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
    
}

// APP进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[EMClient sharedClient] applicationDidEnterBackground:application];
}

// APP将要从后台返回
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[EMClient sharedClient] applicationWillEnterForeground:application];
}
@end

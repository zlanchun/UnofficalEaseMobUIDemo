//
//  ViewController.m
//  UnofficalEaseMobUIDemo
//
//  Created by z on 2017/6/21.
//  Copyright © 2017年 z. All rights reserved.
//

#import "ViewController.h"
#import "EaseUI.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[EMClient sharedClient] loginWithUsername:@"8001"
                                      password:@"111111"
                                    completion:^(NSString *aUsername, EMError *aError) {
                                        if (!aError) {
                                            NSLog(@"登录成功");
                                        } else {
                                            NSLog(@"登录失败");
                                        }
                                    }];
}

- (IBAction)pushToEaseMessageViewController:(id)sender {
    //环信ID:@"8001"
    //聊天类型:EMConversationTypeChat
    EaseMessageViewController *chatController = [[EaseMessageViewController alloc] initWithConversationChatter:@"8001" conversationType:EMConversationTypeChat];
    [self.navigationController pushViewController:chatController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

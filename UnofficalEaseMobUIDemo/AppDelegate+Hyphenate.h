//
//  AppDelegate+Hyphenate.h
//  UnofficalEaseMobUIDemo
//
//  Created by z on 2017/6/21.
//  Copyright © 2017年 z. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Hyphenate)
- (void)setupHyphenateWihtApplication:(UIApplication *)application options:(NSDictionary *)launchOptions;
@end
